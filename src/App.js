import React, { Component } from "react";
import './App.scss';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

const arrModalsValue = [
  {header: "Do you want to delete this file?",
   text: "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?",
   closeButton: true,
   buttonText: {
     okBtn: "Ok",
     cancelBtn: "Cancel"
   }
  },
  {header: "Save file?",
   text: "Are you sure?",
   closeButton: false,
   buttonText: {
    okBtn: "Yes",
    cancelBtn: "No"
  }
  }
]

class App extends Component {
  state = {
    currentModal: undefined,
    isActive: false
  }

  openModal = ({id}) => {
    this.setState({ currentModal : arrModalsValue[id]})
    this.setState({ isActive : true})
  }

  closeModal = () => {
    this.setState({ isActive : false})
  }

  render() {
    const {currentModal, isActive} = this.state
    return (
      <div className="App">
        <div className="btnsWrapper">
          <Button
            backgroundColor="green"
            text="Open first modal"
            handleClick={this.openModal}
            id={0}
            classBtn="mainBtns"
          />
          <Button
            backgroundColor="blue"
            text="Open second modal"
            handleClick={this.openModal}
            id={1}
            classBtn="mainBtns"
          />
        </div>
        
        {isActive && <Modal
          header={currentModal.header}
          text={currentModal.text}
          closeButton={currentModal.closeButton}
          actions={{
            okButton: () => (<Button 
            text={currentModal.buttonText.okBtn}
            handleClick={this.closeModal}
            classBtn="btns-Ok-Cancel--btn"/>),

            cancelButton: () => (<Button
              text={currentModal.buttonText.cancelBtn}
              handleClick={this.closeModal}
              classBtn="btns-Ok-Cancel--btn"/>)
            }}
          isActive={isActive}
          closeModal={this.closeModal}
        />}
      </div>
    );
  }
}

export default App;
